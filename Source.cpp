#include <stdlib.h>
#include <iostream>
#include <string>
#include <string.h>
#include <fstream>
#include "sqlite3.h"
#include <vector>
#include <map>
#include <unordered_map>

using namespace std;




int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	rc = sqlite3_exec(db, "CREATE TABLE people(id integer primary key autoincrement, name TEXT)", NULL, 0, &zErrMsg);//create table with increasing id and name
	rc = sqlite3_exec(db, "INSERT INTO people(name) values('Toyota')", NULL, 0, &zErrMsg);//add name
	rc = sqlite3_exec(db, "INSERT INTO people(name) values('Toyota2')", NULL, 0, &zErrMsg);//add name
	rc = sqlite3_exec(db, "INSERT INTO people(name) values('Toyota3')", NULL, 0, &zErrMsg);//add name
	rc = sqlite3_exec(db, "UPDATE people SET name = 'newname' WHERE name = 'Toyota'", NULL, 0, &zErrMsg);//update name

}

